# DevOps_Tutorial

This project is for just the demonstration of CI/CD and version control.

We will devide the project into two parts.
i)We will make a single master branch and will create an application with the requirement of three features in it.
Each time we will add code for one feature and will commit the code, gitlab runner will trigger the CI/CD operation.
Stages like test and deploy will be executed and the application will be deployed on the Heroku/AWS (TBD)

ii)In second scenerio we will create multiple branches and each of them will have their separate CI/CD pipleline.
After test stage is complete then child branch will merge automatically with the main branch . And finally when all the child branches have been merged then Pipeline 
of the main branch will be executed and after the test stage is complete the application will get deployed on the cloud(Heroku/Aws).
